const path = require('path')
const webpack = require('webpack')
const htmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const CssMiniPlugin = require('css-minimizer-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');



const config = {
    mode: 'development',
    entry: {
        home: path.resolve(__dirname, '../src/mpa/home.js'),
        login: path.resolve(__dirname, '../src/mpa/login.js'),
    },
    output: {
        filename: 'js/[name].js',
        path: path.resolve(__dirname, '../dist')
    },
    devServer: {
        static: {
            directory: path.resolve(__dirname, '../dist'),
        },
        compress: true, // 启动压缩
        port: 9000, // 端口号
        hot: true, // 启动
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset',
                parser: {
                    dataUrlCondition: {
                        maxSize: 8 * 1024
                    }
                },
                generator: {
                    filename: 'images/[name].[hash:6][ext]' // 使用hash防止重名
                }
            },
            {
                test: /\.ejs/,
                loader: 'ejs-loader',
                options: {
                    esModule: false
                }
            }
        ]
    },
    plugins: [
        new htmlWebpackPlugin({
            filename: "home.html", // 模板名称
            template: path.resolve(__dirname, '../public/index.html'),
            chunks: ['home']
        }),
        new htmlWebpackPlugin({
            filename: "login.html", // 模板名称
            template: path.resolve(__dirname, '../public/index.html'),
            chunks: ['login']
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        }),
        new CopyWebpackPlugin({
            // 匹配路径
            patterns: [{
                from: path.resolve(__dirname, '../src/img'),
                to: path.resolve(__dirname, '../dist/img'),
            }]
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
            chunkFilename: 'css/[name].chunk.css',
        }),
        new CleanWebpackPlugin(),
        new VueLoaderPlugin()
    ],
    optimization: {
        minimize: true,
        minimizer: [
            // 构建时 去掉注释
            new TerserPlugin({
                terserOptions: {
                    format: {
                        comments: false,
                    },
                },
                extractComments: false,
            }),
            // css 压缩
            new CssMiniPlugin()
        ],
        splitChunks: {
            minSize: 20 * 1024, // 生成chunk最小体积 
            chunks: 'all', // 所有js文件
            name: 'common', //重命名
            cacheGroups: { // 对某个库进行单独打包
                jquery: {
                    name: 'jquery',
                    test: /jquery/,
                    chunks: 'all'
                },
                'lodash-es': {
                    name: 'lodash-es',
                    test: /lodash-es/,
                    chunks: 'all'
                }
            }
        }
    },
}

module.exports = config