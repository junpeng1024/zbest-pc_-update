const path = require('path')
const { VueLoaderPlugin } = require('vue-loader');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const webpack = require('webpack')


module.exports = function (api, params) {
    console.log('vuexxxx')
    const dir = process.cwd()
    const config = api.getWebpackConfig()

    config.module
        .rule('ejs')
        .test(/\.ejs/)
        .exclude.add('/node_modules/')
        .end()
        .use('ejs-loader')
        .loader('ejs-loader')
        .options({
            esModule: false
        })


    config.plugin('ProvidePlugin')
        .use(webpack.ProvidePlugin, [{
            $: 'jquery',
            jQuery: 'jquery'
        }])

    config.plugin('CopyWebpackPlugin')
        .use(CopyWebpackPlugin, [{
            patterns: [{
                from: path.resolve(dir, './src/img'),
                to: path.resolve(dir, './dist/img'),
            }]
        }])

}